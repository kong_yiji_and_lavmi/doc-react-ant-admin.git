---
title: 常见问题
head:
  - - meta
    - name: description
      content: react-ant-admin 常见问题，详细讲解此框架的分支不同，教你如何掌握框架的工作流程，快速上手。
  - - meta
    - name: keywords
      content: react react-ant react-admin react-ant-admin 常见问题
---

# 常见问题

在使用本框架推荐node版本`14.18.0`以上。使用`cnpm`或者`yarn`等脚本工具进行安装依赖。

```bash
node -v # 14.18.0
```

## 安装报错

* 使用`npm i `安装报错

![npm i 安装报错](https://z3web.cn/images/code/install_1.png)

安装出现报错，请查看报错信息，例如以上信息是因为`less`与`less-loader` 版本依赖冲突所以导致报错，因此可以更改`package.json`里面`less`版本为`3.0.0`，然后删除`node_modules`文件夹以及`package-lock.json`文件，重新**安装依赖**即可。（其它报错原因，请使用yarn等脚本工具安装）



## 启动报错

* 脚本启动报错1

![启动报错1](https://z3web.cn/images/code/start_1.jpg)
![启动报错2](https://z3web.cn/images/code/start_2.jpg)


以上出错是因为 `node` 版本问题所致，因为[vite分支](https://gitee.com/kong_yiji_and_lavmi/react-ant-admin/tree/vite/)最低需要`14.18.*`以上版本才支持！请升级或迁移node合适版本，删除`node_modules`文件夹以及`package-lock.json`文件，重新**安装依赖**即可。[怎样升级node版本？](https://cn.bing.com/search?q=%E5%8D%87%E7%BA%A7node%E7%89%88%E6%9C%AC)

* 脚本启动报错2

![启动报错4](https://z3web.cn/images/code/start_4.jpg)

以上出错是因为 `node` 版本问题所致，当 `nodejs` 升级到17+版本以后，开始支持 `OpenSSL 3.0`，而 `OpenSSL 3.0` 对各种摘要算法做了更严格的限制，可能会导致一些程序运行错误。解决办法如下：

```json
// package.json 文件中 修改启动命令
{
 
  "scripts": {
    "start": "set NODE_OPTIONS=--openssl-legacy-provider && node color && env-cmd --verbose -e development node scripts/start.js",
    // 在命令前加上 set NODE_OPTIONS=--openssl-legacy-provider
  },
}

```

[这里查看讲解文章](https://juejin.cn/post/7202639428132044858)


* 脚本启动成功，但是页面打不开？

![页面打不开](https://z3web.cn/images/code/start_3.jpg)

页面一直圈圈，请检查`浏览器控制台`**网络**一项是否加载js，css异常，遇到以上情况请检查网络(全局)代理是否有问题。若无问题，请更换启动端口号如：`5147`,`3001`。


## 请求报错


![前端请求报错](https://z3web.cn/images/code/network_1.png)

遇到此情况，请检查看`浏览器控制台`**网络**一项，xhr请求是否走了代理，若请求代理请更正请求地址。使用`webpack`分支启动会遇见这种情况，请按照[ajax配置](/webpack/ajax)解决此问题。`vite`分支默认请求线上 http://z3web.cn 不存在请求报错。