---
title: 环境变量预设
head:
  - - meta
    - name: description
      content: react-ant-admin 文件配置，详细讲解此框架的文件部署，教你如何掌握框架的工作流程，快速上手。
  - - meta
    - name: keywords
      content: react react-ant react-admin react-ant-admin. 环境变量预设
---

# 环境变量预设

在项目中 我们想通过一些预设变量，在构建工具中通过`process.env.xxx`来访问预设变量，达到不同的构建效果。本项目使用[env-cmd](https://www.npmjs.com/package/env-cmd)第三方库来设定变量。下面将讲解本项目用到的预设变量，文件所在位置：`your project/.env-cmdrc.js`。

::: tip
如果你想加入自定义的`ENV`变量，请以`REACT_APP_`开头！
:::

## webpack分支预设变量

```js
// webpack分支下 .env-cmdrc.js 文件内容
const devConfig = {
  PORT: 3000, // 启动端口
  HOST: "0.0.0.0", // 监听地址
  NODE_ENV: "development", // 开发者模式
  REACT_APP_ROUTERBASE: "/react-ant-admin", // react路由基础路径
  REACT_APP_API_BASEURL: "http://127.0.0.1:8081/api/react-ant-admin", //请求地址
  PUBLIC_URL: "/react-ant-admin", // 静态文件路径
};
const productionCfg = {
  REACT_APP_ROUTERBASE: "/react-ant-admin", // react路由基础路径
  REACT_APP_API_BASEURL: "/api/react-ant-admin", //请求地址
  PUBLIC_URL: "/react-ant-admin", // 静态文件路径
  NODE_ENV: "production", // 打包模式 生产模式
  BUILD_PATH: "react-ant-admin", // 打包 文件夹名称
};
module.exports = Promise.resolve({
  // 本地接口正常运行 没有mock 没有 主题色
  development: devConfig,

  // 本地接口  启用主题色运行
  development_color: {
    ...devConfig,
    COLOR: "true", // "true" 为 启动
  },

  // 本地mock  运行
  development_mock: {
    ...devConfig,
    REACT_APP_MOCK: "1", // 1 为开启mock
  },

  // 主题色 和 本地mock  运行
  development_color_mock: {
    ...devConfig,
    COLOR: "true",
    REACT_APP_MOCK: "1",
  },

  // 打包 ：无主题 无mock
  production: productionCfg,

  // 打包 ： 有主题 无mock
  production_color: {
    ...productionCfg,
    COLOR: "true", // "true" 为 启动
  },

  // 打包 ： 有主题 有mock  纯本地模式打包
  production_color_mock: {
    ...productionCfg,
    COLOR: "true",
    REACT_APP_MOCK: "1",
  },

  // GitHub pages 打包  博主使用
  production_github: {
    ...productionCfg,
    COLOR: "true",
    REACT_APP_API_BASEURL: "https://azhengpersonalblog.top/api/react-ant-admin",
    REACT_APP_ROUTER_ISHASH: "1", // 启用哈希模式
    REACT_APP_ROUTERBASE: "/",
  },
});
```

以下预设变量是本项目中用到的。

- 开发模式(dev)

```js
const dev = {
  PORT: 3000, // 项目启动端口
  HOST: "0.0.0.0", // 项目监听地址
  NODE_ENV: "development", // 开发者模式
  REACT_APP_ROUTERBASE: "/react-ant-admin", // react路由基础路径
  REACT_APP_API_BASEURL: "http://127.0.0.1:8081/api/react-ant-admin", //api请求基础地址
  PUBLIC_URL: "/react-ant-admin", // 静态文件路径 启动时 默认以 localhost:3000
  REACT_APP_ROUTER_ISHASH: "1", // 不存在 或者 不为 1  使用 history 模式 反之 启用 hash 模式
  COLOR: "true", // 不存在 或者 不为 true 不启用主题色 反之 启用
  REACT_APP_MOCK: "1", // 不存在 或者 不为 1 不启用mock本地模式 反之 启用
};
```

- 打包模式(production)

```js
const pro = {
  REACT_APP_ROUTERBASE: "/react-ant-admin",
  REACT_APP_API_BASEURL: "/api/react-ant-admin",
  PUBLIC_URL: "/react-ant-admin",
  NODE_ENV: "production",
  BUILD_PATH: "react-ant-admin", // 打包文件路径名
  REACT_APP_MOCK: "1",
  COLOR: "true",
  REACT_APP_ROUTER_ISHASH: "1",
};
```

- 在项目中访问

通常都是以`process.env.REACT_APP_xxx` 访问预设变量。

## vite分支预设变量

```js
// vite分支下 .env-cmdrc.js 文件内容
const dev = {
  REACT_APP_ROUTERBASE: "/react-ant-admin",
  REACT_APP_API_BASEURL: "/api",
  REACT_APP_MODE: "development",
}
const REACT_APP_ROUTER_ISHASH = "1"
const pro = {
  REACT_APP_ROUTERBASE: "/react-ant-admin",
  REACT_APP_API_BASEURL: "/api/react-ant-admin",
  REACT_APP_MODE: "production"
}
const REACT_APP_MOCK = "1"
const REACT_APP_COLOR = "1"

module.exports = Promise.resolve({
  dev: dev,
  dev_mock: {
    ...dev,
    REACT_APP_MOCK
  },
  dev_color: {
    ...dev,
    REACT_APP_COLOR
  },
  build: pro,
  build_color: {
    ...pro,
    REACT_APP_COLOR
  }
})
```

以下预设变量是本项目中用到的。

```json
{
  "REACT_APP_ROUTERBASE": "/react-ant-admin", // react路由基础路径
  "REACT_APP_API_BASEURL": "/api", //api请求基础地址
  "REACT_APP_ROUTER_ISHASH": "1", // 不存在 或者 不为 1  使用 history 模式 反之 启用 hash 模式
  "REACT_APP_COLOR": "true", // 不存在 或者 不为 true 不启用主题色 反之 启用
  "REACT_APP_MOCK": "1", // 不存在 或者 不为 1 不启用mock本地模式 反之 启用
}
```

- 在项目中访问

在`vite.config.ts`文件中通过`process.env.REACT_APP_xxx`访问预设变量。在src文件夹下（文件为经过vitejs打包的文件），通常使用`import.meta.env.REACT_APP_xxx`访问预设变量。[更多内容请参考这里](https://cn.vitejs.dev/guide/env-and-mode.html#env-files)