---
title: 主题色(webpack)
head:
  - - meta
    - name: description
      content: react-ant-admin 主题色(webpack)，详细讲解此框架主题色，快速掌握动态切换主题，让页面焕然一新。
  - - meta
    - name: keywords
      content: react react-ant react-admin react-ant-admin 主题色(webpack).
---

# 主题色配置

主题色功能是利用less变量，去动态替换掉本地项目的less变量的值。达到主题色切换，客户端上是利用了`less.min.js`调用`window.less.modifyVars`去修改用到的变量样式。具体内容请参考[这里](http://lesscss.cn/usage/#using-less-in-the-browser)。

::: tip
注意：使用主题色，在项目启动的预设环境变量`COLOR`必须为`"true"`，否则主题色功能会未被开启。
:::

## 新增less变量

在`src/assets/theme/var.less`文件里新增less变量，即可在`.less`文件中使用。

```less
// 脚本抓取变量 -- 开始
// script-start
/**
  在这里写自定义less全局变量。参照一下格式 颜色值最好为十六进制或者rgb格式 如： #000 rgb(0,2,0)
  因为用优先级关系，antd的变量放在下面，最上面的优先级最大
**/
// 布局
@layout-aside-activeBg: #e6f7ff; // 侧边栏选中背景色
@test                 : red; // 测试颜色
@test-1               : blue; // 测试颜色2
@import "~antd/lib/style/themes/default.less";
// script-end

// 脚本抓取变量 -- 结束

```

## 新增变量页面刷新还是没有？

每次新增需要重新构建，因为项目启动会执行`color.js`脚本，提取出自定义的less变量，再去替换项目里面用到的less变量。

