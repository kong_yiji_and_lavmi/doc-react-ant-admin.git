---
title: 文件分布(webpack)
head:
  - - meta
    - name: description
      content: react-ant-admin 文件分布(webpack)，详细讲解此框架文件分布，快速掌握文件作用，提高开发速度。
  - - meta
    - name: keywords
      content: react react-ant react-admin react-ant-admin 文件分布(webpack).
---

# 文件分布

```bash
src
 ├─ api
 │   └─ index.js    网络API接口文件
 │
 ├─ asset           静态文件资源
 │
 ├─ common          配置信息
 │    ├─ index.js   主要信息配置文件
 │    └─ ajax.js    网络请求库配置文件
 │
 ├─ components      自定义组件库
 │
 ├─ layout          项目页面布局
 │    ├─ mode       布局模式文件夹
 │    ├─ footer.js  项目页面底部布局
 │    ├─ header.js  项目页面头部布局
 │    ├─ index.js   项目页面主题布局
 │    ├─ index.less 布局样式
 │    ├─ siderMenu.js 项目页面侧边栏菜单布局
 │    └─ topMenu.js  项目页面顶部栏菜单布局
 │
 ├─ mock
 │   └─ index.js    本地模拟数据文件
 │
 ├─ pages           项目页面文件夹
 │
 ├─ router             项目路由文件夹
 │    ├─ appRouter.js  项目App主要路由
 │    ├─ index.js      项目页面信息，菜单信息组成渲染
 │    ├─ intercept.js  路由权限拦截文件
 │    └─ list.js       页面路由列表
 │
 ├─ store           redux全局数据管理
 │    ├─ layout     关于布局模式
 │    ├─ menu       关于菜单
 │    ├─ user       关于用户
 │    ├─ visibel    关于用户显示
 │    └─ index.js   入口文件
 │
 ├─ utils           工具库
 ├─ App.js          项目渲染主入口
 ├─ index.js        项目启动主入口
 └─ setupProxy.js   跨域转发配置文件
```
