module.exports = {
  title: "react-ant-admin",
  base: "/doc-react-ant-admin/",
  dest: "public",
  lang: "zh-CN",
  lastUpdated: true,
  description: "适用于开发人员快速搭建中后台页面，管理系统。",
  hmr: { overlay: false },
  head: [
    [
      "meta",
      {
        name: "viewport",
        content:
          "width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no",
      },
    ],
    ["link", { rel: "icon", href: "/doc-react-ant-admin/logo.svg" }],
    // 统计代码
    [
      "script",
      { src: "https://hm.baidu.com/hm.js?3a995a34b126c8652c75166fa2690df9" },
    ],
    // 百度推送代码
    [
      "script",
      { src: "/doc-react-ant-admin/push.js" },
    ],
    [
      "script",
      { src: "https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-4780421798583301", crossorigin: 'anonymous', async: 'async' },
    ]
  ],

  themeConfig: {
    sidebar: getSidebar(),
    algolia: {
      appId: 'AL1QFC6D6J',
      apiKey: 'afb33711192aecd970cac3e08b5f26d6',
      indexName: 'react-ant-admin'
    },
    author: "kongyijilafumi",
    nav: getNav(),
  },
  markdown: {
    lineNumbers: true
  }

};

function getSidebar() {
  return [
    {
      text: "介绍",
      items: [
        { text: "什么是react-ant-admin？", link: "/guide/" },
        { text: "开始使用", link: "/guide/start" },
        { text: "环境变量预设", link: "/guide/env" },
        { text: "分支介绍", link: "/guide/branch" },
      ],
    },
    {
      text: "webpack分支",
      items: [
        { text: "请求配置", link: "/webpack/ajax" },
        { text: "路径配置", link: "/webpack/path" },
        { text: "文件分布", link: "/webpack/src" },
        { text: "菜单信息", link: "/webpack/menu" },
        { text: "主题色配置", link: "/webpack/theme" },
      ]
    },
    {
      text: "vite分支",
      items: [
        { text: "请求配置", link: "/vite/ajax" },
        { text: "路径配置", link: "/vite/path" },
        { text: "文件分布", link: "/vite/src" },
        { text: "菜单信息", link: "/vite/menu" },
        { text: "主题色配置", link: "/vite/theme" },
      ]
    },
    {
      text: "侧边栏菜单与页面",
      items: [
        { text: "创建一个侧边栏菜单", link: "/menu/createMenu" },
        { text: "创建一个页面", link: "/menu/createPage" },
      ]
    },
    {
      text: "权限信息配置",
      items: [
        { text: "权限配置", link: "/power/", },
        { text: "用户权限配置", link: "/power/user" },
        { text: "菜单如何关联用户权限", link: "/power/menu" },
      ],
    },
    {
      text: "常见问题",
      items: [
        { text: "常见问题", link: "/faq/" }
      ]
    },
    {
      text: "留言",
      items: [{ link: "/feedback/", text: "留言板" }]
    },
  ];
}

function getNav() {
  return [
    { text: "介绍", link: "/guide/", activeMatch: "^/guide/" },
    { text: "常见问题", link: "/faq/", activeMatch: "/faq/" },
    { text: "留言", link: "/feedback/", activeMatch: "/feedback/" },
    {
      text: "预览地址",
      link: "https://z3web.cn/react-ant-admin/",
    },
    { text: "更多地址", link: "/contact/", activeMatch: "/contact/" },
  ];
}
