import { onMounted } from 'vue'

export default function fetchReleaseTag() {
  onMounted(() => {
    fetch('https://api.github.com/repos/kongyijilafumi/react-ant-admin/releases/latest')
      .then((res) => res.json())
      .then((json) => {
        const mainTitle = document.querySelector('.main .name')
        mainTitle.style.position = 'relative'

        const docsReleaseTag = document.createElement('span')
        docsReleaseTag.classList.add('release-tag')
        const releaseTagName = json.tag_name
        docsReleaseTag.innerText = releaseTagName

        if (releaseTagName !== undefined) {
          mainTitle.appendChild(docsReleaseTag)
        }
      })
  })
}
