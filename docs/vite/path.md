---
title: 路径配置(vite)
head:
  - - meta
    - name: description
      content: react-ant-admin 路径配置(vite)，详细讲解此框架的文件部署，教你如何掌握框架的工作流程，快速上手。
  - - meta
    - name: keywords
      content: react react-ant react-admin react-ant-admin 路径配置(vite)
---

# 路径配置

## 如何设置指定某地址为默认打开地址

在项目启动后，默认打开的地址是`/`，若要设置为其他路由地址，可以在`src/router/list.tsx`文件中配置`defaultArr`变量即可

```js
// ./src/router/list.tsx
import { Navigate } from "react-router-dom";
const defaultArr: RouterInfo[] =  = [
  {
    [MENU_PATH]: "/", // 首页地址
    [MENU_KEY]: "index",
    components: <Navigate to="/details/person" replace />, // 跳转路由组件 去 /details/person
  },
  // .......
];

```

## 修改项目 base 路由前缀

找到`.env-cmdrc.js`文件进行如下修改即可。

```js
const devConfig = {
  REACT_APP_ROUTERBASE: "/abc", // react路由基础路径 /abc
};
```

## 修改打包生产的文件夹名称

在`vite.config.ts`文件中配置`build.outDir`即可。

```js
import { defineConfig } from 'vite'
export default defineConfig({
  build: {
    outDir: "react-ant-admin"
  },
})
```

## 静态资源目录地址

在`vite.config.ts`文件中配置`base`即可。

```js
export default defineConfig({
  build: {
    outDir: "react-ant-admin"
  },
})
```

## 打包之后静态资源加载失败

因为`create-react-app`打包，默认把镜头资源指向 `url`的根路径。

- 举例

假设你的网站地址为：https://wwww.xxxxx.com

现在你想把项目放在这个网站的二级域名下如：https://wwww.xxxxx.com/blog 但是你没有修改`vite.config.ts`里的**base**属性，所以就会发现静态文件 404。因为它们都以根目录去访问。所以会出现上述情况。

解决这种办法去修改`vite.config.ts`里的**base**属性即可



::: tip
希望`vite.config.ts`里的**base**属性,与`vite.config.ts`文件中配置`build.outDir`，这两个的值相同，否则打包都会发生意想不到的事情!

:::

## React-Router 使用哈希模式(#)

只需要在`.env-cmdrc.js` 的启动模式下加上`REACT_APP_ROUTER_ISHASH`字段并且值为`1`即可。

```json
{
 "REACT_APP_ROUTER_ISHASH": "1", // 启用哈希模式
}
```
