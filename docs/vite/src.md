---
title: 文件分布(vite)
head:
  - - meta
    - name: description
      content: react-ant-admin 文件分布(vite)，详细讲解此框架文件分布，快速掌握文件作用，提高开发速度。
  - - meta
    - name: keywords
      content: react react-ant react-admin react-ant-admin 文件分布(vite).
---

# 文件分布


```bash
src
 ├─ api
 │   └─ index.ts    网络API接口文件
 │
 ├─ asset           静态文件资源
 │
 ├─ common          配置信息
 │    ├─ index.ts   主要信息配置文件
 │    └─ ajax.ts    网络请求库配置文件
 │
 ├─ components      自定义组件库
 │
 ├─ hooks           react自定义的hooks
 │
 ├─ layout          项目页面布局
 │
 ├─ mock
 │   └─ index.ts    本地模拟数据文件
 │
 ├─ pages           项目页面文件夹 # 与webpack版本一样
 │
 ├─ router          项目路由文件夹 # 与webpack版本一样
 │
 ├─ store           redux全局数据管理  
 │    ├─ getter     获取store某属性的值
 │    ├─ hooks      关于store的hooks
 │    ├─ layout     关于布局模式
 │    ├─ menu       关于菜单
 │    ├─ theme      关于主题色 # 新增
 │    ├─ user       关于用户
 │    ├─ visibel    关于页面显示
 │    └─ index.js   入口文件
 │
 ├─ types           项目ts类型
 ├─ utils           工具库
 ├─ App.tsx          项目渲染主入口
 ├─ main.tsx        项目启动主入口
 └─ vite-env.d.ts   项目ts类型预设文件
```
