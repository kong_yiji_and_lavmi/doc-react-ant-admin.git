---
title: 主题色(webpack)
head:
  - - meta
    - name: description
      content: react-ant-admin 主题色(webpack)，详细讲解此框架主题色，快速掌握动态切换主题，让页面焕然一新。
  - - meta
    - name: keywords
      content: react react-ant react-admin react-ant-admin 主题色(webpack).
---

# 主题色配置

主题色功能是通过[antd@5](https://ant-design.gitee.io/docs/react/customize-theme-cn)自带的主题色功能来实现动态切换。

::: tip
注意：使用主题色，在项目启动的预设环境变量`process.env.REACT_APP_COLOR`必须为`"1"`，否则主题色功能会未被开启。使用[antd-style](https://ant-design.github.io/antd-style/guide)自定义less变量。
:::

## 使用antd自带的less Token 变量

```js
// main.js
import { ConfigProvider } from 'antd';
import { theme } from 'antd';
import Router from "./router"
const defaultToken = theme.defaultAlgorithm(theme.defaultSeed)
export default function App(){
  return <ConfigProvider theme={defaultToken}>
    <Router /> {/* 你需要用到的组件 */}
  </ConfigProvider >
}

// router.js
import { createStyles } from 'antd-style';
import { theme } from 'antd';
const themeToken = theme.defaultAlgorithm(theme.defaultSeed)

const createStyle = createStyles((({ css }, token) => ({
  // 这里的 colorBgContainer colorSplit 是来自 antd 
  info: css`
    margin-top    : 20px;
    padding-bottom: 20px;
    border-bottom : 1px dashed ${token.colorSplit};
    text-align    : center;
  `,
  tabs: css`
    padding         : 0 20px;
    background-color: ${token.colorBgContainer};
  `,
})))

export default function Router(){
  // 这里展示 是 静态的 themeToken  在项目中 是存在store里动态的。
  const { styles } =  createStyle(themeToken)
  return <div className={styles.info}>
    <div className={styles.tabs}>111</div>
    <div className={styles.tabs}>222</div>
    <div className={styles.tabs}>333</div>
  </div>
}
```

## 新增自定义less变量

在`your project/theme/var.less`文件里新增less变量，然后就可以使用lessToken了。

```less
@test-color: red;
@test-size : 22;
```
在本项目中直接可以使用，记得新增ts提示。
```ts
//src/theme.ts
interface CustomThemeToken {
  'test-color': string
  'test-size': number
  [key: string]: string | undefined
}
```
```js
import { useThemeToken } from '@/hooks';
import { createStyles } from 'antd-style';
// 这里的 token 是 传参进来的
const createStyle = createStyles((({ css }, token) => ({
  color:css`
    color:${token['test-color']}
  `,
  font:css`
    font-size:${token['test-size']}px
  `
})))


function Comp(){
  const { styles } = createStyle(useThemeToken())
  return <div>
    <p className={styles.color}>使用了test-color</p>
    <span className={styles.font}>使用了test-size</span>
  </div>
}

```

## less文件还可以用less变量吗？

当然可以，你可以使用antd的less变量名称[列表](https://ant.design/docs/react/customize-theme#seedtoken)和 自定义的less变量在`your project/theme/var.less`里面定义。但是，他不会动态改变，因为在启动或者打包的时候，`vite`会自动把`less变量`替换**真正的值**并且转换为`静态的css`，以`style`标签插入文档中，是不会动态改变的。如果你要使用能在页面上动态改变的`less`变量，请以上面那种方式，动态的使用`className`。具体请参考[antd-style](https://ant-design.github.io/antd-style/guide)