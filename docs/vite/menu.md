---
title: 侧边栏菜单信息(vite)
head:
  - - meta
    - name: description
      content: react-ant-admin 侧边栏菜单信息(vite)，详细讲解此框架的文件部署，教你如何掌握框架的工作流程，快速上手。
  - - meta
    - name: keywords
      content: react react-ant react-admin react-ant-admin 侧边栏菜单信息(vite)
---

# 侧边栏菜单信息

::: tip
菜单信息可以通过修改`vite.config.ts`文件的`define`属性中数据格式来转换成自己想要的菜单信息。
注意：`MENU_PATH`等等数据 是可以直接在`src`下(经过vite构建打包的)全局使用的，其原理是利用了[webpack.DefinePlugin](https://www.webpackjs.com/plugins/define-plugin/)变量全局替换使用。该配置详情[查看](https://cn.vitejs.dev/config/shared-options.html#define)
:::

```js
// 属性意义 与 webpack版本差不多
import { defineConfig } from 'vite'
export default defineConfig({
  define: {
    MENU_PATH: `"path"`,
    MENU_SHOW: `"isShowOnMenu"`,
    MENU_KEEPALIVE: `"keepAlive"`,
    MENU_KEY: `"key"`,
    MENU_ICON: `"icon"`,
    MENU_TITLE: `"title"`,
    MENU_CHILDREN: `"children"`,
    MENU_PARENTKEY: `"parentKey"`,
    MENU_ALLPATH: `"allPath"`,
    MENU_PARENTPATH: `"parentPath"`,
    MENU_LAYOUT: `'layout'`,
    __IS_THEME__: `${process.env.REACT_APP_COLOR === "1"}`,
    CUSTOMVARLESSDATA: `${JSON.stringify(customVarLessJson)}`
  },
})
```

- 当在修改 `MENU_***` 数据为以上信息的时候。需要后台返回 对于的菜单信息即可。

```js
const menuList = [
  {
    id: 1,
    name: "主菜单",
    parent_id: 0,
    url: "/parent",
  },
  {
    id: 2,
    name: "子菜单1",
    parent_id: 1,
    url: "/child1",
  },
  {
    id: 3,
    name: "子菜单2",
    parent_id: 1,
    url: "/child2",
  },
];
```

- 数据格式由前端转换自动组成树状结构。使用`src/utils/index.js`里的`formatMenu`方法会自动装换成以下结构。(你可以自己实现)

```json
[
  {
    "id": 1,
    "name": "主菜单",
    "parent_id": 0,
    "url": "/parent",
    "children": [
      // 这里的 children 是通过 MENU_CHILDREN 控制 你可以随意修改成你想要的
      {
        "id": 2,
        "name": "子菜单1",
        "parent_id": 1,
        "url": "/child1"
      },
      {
        "id": 3,
        "name": "子菜单2",
        "parent_id": 1,
        "url": "/child2"
      }
    ]
  }
]
```

::: tip
如果返回的菜单列表，某条菜单信息不存在，则会表示 没有该路径权限。请看下面案例
:::

```json
// 这是 用户 1 的菜单信息 表示 可以访问 /parent/child1  /parent/child2
[
  {
    "id": 1,
    "name": "主菜单",
    "parent_id": 0,
    "url": "/parent"
  },
  {
    "id": 2,
    "name": "子菜单1",
    "parent_id": 1,
    "url": "/child1"
  },
  {
    "id": 3,
    "name": "子菜单2",
    "parent_id": 1,
    "url": "/child2"
  }
]
// 这是 用户 2 的菜单信息 表示 可以访问 /parent/child1  不能访问 /parent/child2  会遭到拦截
[
  {
    "id": 1,
    "name": "主菜单",
    "parent_id": 0,
    "url": "/parent"
  },
  {
    "id": 2,
    "name": "子菜单1",
    "parent_id": 1,
    "url": "/child1"
  },
]
```
