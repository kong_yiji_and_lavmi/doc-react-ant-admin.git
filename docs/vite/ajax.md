---
title: ajax配置(vite)
head:
  - - meta
    - name: description
      content: react-ant-admin ajax配置(vite)，详细讲解此框架ajax设置，教你解决前端请求问题。
  - - meta
    - name: keywords
      content: react react-ant react-admin react-ant-admin ajax(vite).
---

# 请求配置

## 解决跨域

`vite`自带解决跨域问题，在`vite.config.ts`文件中配置`server.proxy`。[更多信息查看这里](https://cn.vitejs.dev/config/server-options.html#server-proxy)

```js
import { defineConfig } from 'vite'
export default defineConfig({
   server: {
    proxy: {
      "^/api": {
        target: "https://z3web.cn",
        changeOrigin: true,
        rewrite: (path) => {
          return path.replace("/api", "/api/react-ant-admin")
        }
      },
    },
  },
})
```

::: warning
注意两点：

1. axios 的`BASE_URL`只能为`/`或者不填。不能为某一地址如：`http://xxx.xxx:xxx/xxx`
2. 如果以上的`prevUrl`为`^/api`的时候，使用 axios 调用`post,get,等等`请求时，必须加上前缀`/api`,例如：`axios.post("/api/login")`

:::

更多说明请看[http-proxy-middleware 文档](https://www.npmjs.com/package/http-proxy-middleware)


## get 请求支持自动拼接 url

因为使用[qs](https://www.npmjs.com/package/qs)这个库,在 get 请求上可以直接使用`Object`类型转化为 url 的拼接参数

```js
import ajax from "@/common/ajax";

const data = {
  name: "kongyijilafumi",
  age: 22,
};
ajax.get("/getUserInfo", data); // /getUserInfo?name=kongyijilafumi&age=22
```

更多信息请查看[qs 文档](https://www.npmjs.com/package/qs)
