---
title: 创建一个菜单栏
head:
  - - meta
    - name: description
      content: react-ant-admin 创建一个菜单栏，详细讲解此框架的文件部署，教你如何掌握框架的工作流程，快速上手。
  - - meta
    - name: keywords
      content: react react-ant react-admin react-ant-admin 创建一个菜单栏
---

# 创建一个菜单栏

若使用请求返回菜单信息的直接向后台添加一条菜单信息即可。

##  mock 模式

在`src/mock/index.js` 找到`menu`变量,往里添加一条菜单信息.代码如下所示

```js
let menu = [
  {
    [MENU_TITLE]: "列表页",
    [MENU_PATH]: "/list",
    [MENU_KEY]:  9,
    [MENU_PARENTKEY]: null,
    [MENU_ICON]: "icon_list",
    [MENU_KEEPALIVE]: "false",
    [MENU_ORDER]: 1,
  },
  {
    [MENU_TITLE]: "卡片列表",
    [MENU_PATH]: "/card",
    [MENU_KEY]: 10,
    [MENU_PARENTKEY]: 9,
    [MENU_ICON]: null,
    [MENU_KEEPALIVE]: "false",
    [MENU_ORDER]: 5485,
  },
  // .... 开始添加菜单信息 ....
  {
    [MENU_TITLE]: "test", // 菜单栏标题，页面title 必要
    [MENU_PATH]: "/test", // 路由路径信息 必要
    [MENU_KEY]: 3, // 唯一值，必要
    [MENU_PARENTKEY]: null, // 空表示 为主菜单而非子菜单
    [MENU_ICON]: null, // 菜单图标 非必要
    [MENU_ORDER]: 3, // 菜单排序 越小越靠前
    [MENU_KEEPALIVE]: "false", // 该页面是否缓存，切换页面时候保存状态
  },
];
```

由于菜单会走本地会话存储`window.sessionStorage`,所以保存代码后需要关闭当前窗口,重新打开页面输入地址 `http://localhost:3000/react-ant-admin`

打开之后,会发现菜单会多出一个`test`栏目,这样就完成了菜单的添加.

## 为啥新增了没有显示出来？

因为`菜单`与`权限`绑定，如果不是`mock`模式。正常逻辑是`请求接口`返回菜单列表。前端只需要根据数据去渲染出该列表的层级关系即可。
